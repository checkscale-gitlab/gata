package com.odroid.gata.spring;

import com.odroid.gata.domain.Authority;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ROOT_URL = "/";
    private static final String RES_URL_PATTERN = "/res/**";
    private static final String WEBJARS_URL_PATTERN = "/webjars/**";
    private static final String USERS_URL_PATTERN = "/usersTab/**";
    private static final String LOGOUT_URL = "/logout";
    private static final String SESSION_COOKIE = "JSESSIONID";
    private static final String USERNAME_PARAMETER = "username";
    private static final String PASS_PARAMETER = "password";

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http.
                authorizeRequests()
                    .antMatchers(ROOT_URL, RES_URL_PATTERN, WEBJARS_URL_PATTERN, "/login")
                        .permitAll()
                    .antMatchers(USERS_URL_PATTERN).hasRole(Authority.ROLE_ADMIN.getShortForm())
                    .anyRequest().hasRole(Authority.ROLE_USER.getShortForm())
                .and().formLogin()
                    .loginPage(MvcConfig.LOGIN_URL)
                    .defaultSuccessUrl(MvcConfig.NOTESTAB_URL)
                    .loginProcessingUrl(MvcConfig.LOGIN_URL)
                    .usernameParameter(USERNAME_PARAMETER)
                    .passwordParameter(PASS_PARAMETER)
                    .permitAll()
                .and().logout()
                    .logoutUrl(LOGOUT_URL)
                    .logoutSuccessUrl(ROOT_URL)
                    .deleteCookies(SESSION_COOKIE)
                    .permitAll()
                .and().csrf()
                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        // @formatter:on
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // @formatter:off
        auth
                .jdbcAuthentication()
                    .dataSource(dataSource)
                    .usersByUsernameQuery("SELECT username, hash, enabled FROM users WHERE username=?")
                    .authoritiesByUsernameQuery("SELECT username, authority FROM user_authorities WHERE username=?")
                    .passwordEncoder(passwordEncoder());
        // @formatter:on
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
