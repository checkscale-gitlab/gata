package com.odroid.gata.spring;

import com.odroid.gata.common.GataBuildProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SpringListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringListener.class);

    private GataBuildProperties gataBuildProperties;

    public SpringListener(GataBuildProperties gataBuildProperties) {
        this.gataBuildProperties = gataBuildProperties;
    }

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        LOGGER.info("Spring context loaded for: {}", gataBuildProperties);
    }
}
