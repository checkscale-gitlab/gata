package com.odroid.gata.rest;

import com.odroid.gata.service.iface.GataException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class GataExceptionHandler extends ResponseStatusExceptionResolver {

    private MessageSource messageSource;

    public GataExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(GataException.class)
    public ModelAndView gataExceptionHandler(HttpServletRequest request, HttpServletResponse response, @Nullable Object handler, GataException e) {
        String message = messageSource.getMessage(e.getI18nCode(), e.getArgs(), e.getI18nCode(), LocaleContextHolder.getLocale());
        return doResolveException(request, response, handler, new ResponseStatusException(HttpStatus.BAD_REQUEST, message, e));
    }

}
