package com.odroid.gata.service.impl;

import com.odroid.gata.service.iface.GataException;

public class ForbiddenException extends GataException {

    private static final String I18N_FORBIDDEN = "i18n.error.common.forbidden";

    public ForbiddenException() {
        super(I18N_FORBIDDEN);
    }
}
