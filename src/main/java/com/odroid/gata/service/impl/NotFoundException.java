package com.odroid.gata.service.impl;

import com.odroid.gata.service.iface.GataException;

public class NotFoundException extends GataException {

    private static final String I18N_NOT_FOUND = "i18n.error.common.notFound";

    public NotFoundException(String entity, Long id) {
        super(I18N_NOT_FOUND, entity, id);
    }
}
