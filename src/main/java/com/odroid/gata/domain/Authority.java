package com.odroid.gata.domain;

public enum Authority {
    ROLE_ADMIN,
    ROLE_USER;

    public String getShortForm() {
        return name().replace("ROLE_", "");
    }
}
