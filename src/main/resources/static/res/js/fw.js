var fwService = gataApp.service('fw', function ($log) {
    var self = this;
    this.debug = {};
    this.debug.var_dump = function (o) {
      var text = "";
      for (var n in o) {
        text += n + ": " + o[n] + "\n";
      }
      alert(text);
    };
    this.debug.var_console = function (o) {
      var text = "";
      for (var n in o) {
        text += n + ": " + o[n] + "\n";
      }
      $log.debug(text);
    };
    this.ajax = {};
    this.ajax.onErrorAction = null;
    this.ajax.onError = function(onErrorAction) {
      self.ajax.onErrorAction = onErrorAction;
    };
    this.ajax.httpError = function (response) {
      self.ajax.onErrorAction(response.data);
    };
    this.ajax.nonConcurrentExecTags = {};
    this.ajax.nonConcurrentExec = function (tag, func) {
      if (tag in this.nonConcurrentExecTags) {
        $log.debug("Exec tag " + tag + ": execution found, setting/replacing next execution");
        this.nonConcurrentExecTags[tag] = func;
        return;
      } else {
        $log.debug("Exec tag " + tag + ": execution not found, executing");
        this.nonConcurrentExecTags[tag] = null;
        func();
      }
    };
    this.ajax.nonConcurrentExecFinish = function (tag) {
      if (this.nonConcurrentExecTags[tag] === null) {
        $log.debug("Exec tag " + tag + ": next execution not found, ending loop");
        delete this.nonConcurrentExecTags[tag];
        return;
      } else {
        $log.debug("Exec tag " + tag + ": next execution found, executing");
        var func = this.nonConcurrentExecTags[tag];
        this.nonConcurrentExecTags[tag] = null;
        func();
      }
    };
    this.ajax.nonConcurrentExecFinishError = function (tag, response) {
        self.ajax.nonConcurrentExecFinish(tag);
        self.ajax.httpError(response);
    };

  });
