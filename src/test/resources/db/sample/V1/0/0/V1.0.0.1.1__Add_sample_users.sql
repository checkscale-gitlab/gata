INSERT INTO users(id, username, hash, enabled, name) VALUES (3, 'c', '$2a$10$AXBgkA6CnnPiahZvT6XHYeKHNpPgE6TyqT.f3QrT3ry4I1GB2v1wy', false, 'Carlos Casado Castro');
INSERT INTO user_authorities(username, authority) VALUES ('c', 'ROLE_USER');
