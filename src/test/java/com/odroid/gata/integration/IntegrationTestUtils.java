package com.odroid.gata.integration;

import com.sun.security.auth.UserPrincipal;

import java.security.Principal;

public abstract class IntegrationTestUtils {

    public static final String USERNAME_ADMIN = "a";
    public static final Long ID_ADMIN = 1L;

    public static final String USERNAME_USER = "b";
    public static final Long ID_USER = 2L;

    private IntegrationTestUtils() {
        // Intentionally blank to avoid instantiation
    }

    public static Principal adminPrincipal() {
        return new UserPrincipal(USERNAME_ADMIN);
    }

    public static Principal userPrincipal() {
        return new UserPrincipal(USERNAME_USER);
    }

}
