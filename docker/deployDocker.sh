#!/bin/bash
cd "$(dirname "$0")"
docker container stop gata
docker container rm gata
docker image rm gata
docker load < gata-docker.tgz
docker container create -m 192m -p 5180:8080 --restart unless-stopped --name gata gata
docker container start gata
